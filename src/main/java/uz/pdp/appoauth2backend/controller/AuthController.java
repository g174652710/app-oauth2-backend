package uz.pdp.appoauth2backend.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appoauth2backend.entity.User;
import uz.pdp.appoauth2backend.payload.ApiResponse;
import uz.pdp.appoauth2backend.payload.SignDTO;
import uz.pdp.appoauth2backend.payload.SignInDTO;
import uz.pdp.appoauth2backend.payload.TokenDTO;
import uz.pdp.appoauth2backend.repository.UserRepository;
import uz.pdp.appoauth2backend.service.AuthService;
import uz.pdp.appoauth2backend.utils.AppConstants;

@RestController
@RequestMapping(AuthController.BASE_PATH)
@RequiredArgsConstructor
public class AuthController {

    public static final String BASE_PATH = AppConstants.BASE_PATH + "/auth";
    public static final String SIGN_UP_PATH = "sign-up";
    public static final String SIGN_IN_PATH = "sign-in";
    public static final String EMAIL_VERIFICATION_PATH = "email-verification/{code}";

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthService authService;

    @PostMapping(SIGN_UP_PATH)
    public String signUp(@RequestBody @Valid SignDTO signDTO) {
        if (userRepository.existsByPhoneNumber(signDTO.getPhoneNumber()))
            return "Oka bunday phoneNumber avval qo'shilgan";

        User user = new User(
                signDTO.getPhoneNumber(),
                passwordEncoder.encode(signDTO.getPassword())
        );
        userRepository.save(user);
        return "Ok okasi";
    }

    @PostMapping(SIGN_IN_PATH)
    public ApiResponse<TokenDTO> signIn(@RequestBody @Valid SignInDTO signInDTO) {
        return authService.signIn(signInDTO);
    }
//
//    //http:localhost/api/auth/verification-code/UUID
//    @GetMapping(EMAIL_VERIFICATION_PATH)
//    public ApiResponse<String> verificationEmail(@PathVariable String code) {
//        return authService.verificationEmail(code);
//    }
//
//    private void sendVerificationCodeToEmail(User user) {
//        SimpleMailMessage mailMessage
//                = new SimpleMailMessage();
//
//        // Setting up necessary details
//        mailMessage.setFrom(sender);
//        mailMessage.setTo(user.getUsername());
//        mailMessage.setSubject("");
//        mailMessage.setText("CLICK_LINK: " + "http://localhost/api/auth/verification-email/" + user.getVerificationCode());
//
//        // Sending the mail
//        javaMailSender.send(mailMessage);
//    }
}
