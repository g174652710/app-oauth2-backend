package uz.pdp.appoauth2backend.payload;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;

@Getter
public class SignInDTO {

    @NotBlank
    private String phoneNumber;

    @NotBlank
    private String password;
}
