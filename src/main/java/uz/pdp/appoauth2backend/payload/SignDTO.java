package uz.pdp.appoauth2backend.payload;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;

@Getter
public class SignDTO {

    @NotBlank
    private String phoneNumber;

    @NotBlank
    private String password;

    @NotBlank
    private String prePassword;


}
