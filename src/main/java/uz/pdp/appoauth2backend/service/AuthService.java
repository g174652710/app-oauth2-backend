package uz.pdp.appoauth2backend.service;

import jakarta.validation.Valid;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.appoauth2backend.entity.User;
import uz.pdp.appoauth2backend.payload.ApiResponse;
import uz.pdp.appoauth2backend.payload.SignInDTO;
import uz.pdp.appoauth2backend.payload.TokenDTO;
import uz.pdp.appoauth2backend.repository.UserRepository;
import uz.pdp.appoauth2backend.security.JWTProvider;
import uz.pdp.appoauth2backend.security.UserPrincipal;

@Service
public class AuthService implements UserDetailsService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final JWTProvider jwtProvider;


    public AuthService(UserRepository userRepository,
                       @Lazy AuthenticationManager authenticationManager,
                       @Lazy PasswordEncoder passwordEncoder, JWTProvider jwtProvider) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.jwtProvider = jwtProvider;
    }

    public ApiResponse<TokenDTO> signIn(@Valid SignInDTO signDTO) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        signDTO.getPhoneNumber(),
                        signDTO.getPassword())
        );
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        return ApiResponse.successResponse(
                TokenDTO.builder()
                        .accessToken(jwtProvider.generateAccessToken(userPrincipal))
                        .refreshToken(jwtProvider.generateRefreshToken(userPrincipal))
                        .build()
        );
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByPhoneNumber(username).orElseThrow(() -> new UsernameNotFoundException("User not found: " + username));
        return new UserPrincipal(user);
    }

}