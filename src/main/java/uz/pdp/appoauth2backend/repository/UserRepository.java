package uz.pdp.appoauth2backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appoauth2backend.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    boolean existsByPhoneNumber(String phoneNumber);

    Optional<User> findByPhoneNumber(String username);

    Optional<User> findByGoogleUsername(String username);

    Optional<User> findByGithubUsername(String username);
}
