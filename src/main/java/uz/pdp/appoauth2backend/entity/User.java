package uz.pdp.appoauth2backend.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.pdp.appoauth2backend.entity.template.AbsUUIDEntity;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbsUUIDEntity {

    @Column(unique = true)
    private String phoneNumber;

    private String password;

    @Column(unique = true)
    private String googleUsername;

    @Column(unique = true)
    private String githubUsername;

    private String name;

    private String imageUrl;

    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;

    public User(String phoneNumber, String password) {
        this.phoneNumber = phoneNumber;
        this.password = password;
    }


}
