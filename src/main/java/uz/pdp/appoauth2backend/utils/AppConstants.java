package uz.pdp.appoauth2backend.utils;

import uz.pdp.appoauth2backend.controller.AuthController;

public interface AppConstants {

    String[] OPEN_PAGES = {
            AuthController.BASE_PATH + "/**",
    };

    String AUTH_HEADER = "Authorization";
    String AUTH_TYPE_BASIC = "Basic ";
    String AUTH_TYPE_BEARER = "Bearer ";

    String BASE_PATH = "/api";
    String ACCESS_TOKEN = "AccessToken";
    String REFRESH_TOKEN = "RefreshToken";
}
