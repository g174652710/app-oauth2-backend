package uz.pdp.appoauth2backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppOauth2BackendApplication {
    public static void main(String[] args) {
        SpringApplication.run(AppOauth2BackendApplication.class, args);
    }
}
