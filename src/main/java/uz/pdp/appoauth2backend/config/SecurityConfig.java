package uz.pdp.appoauth2backend.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import uz.pdp.appoauth2backend.security.oauth2.HttpCookieOAuth2AuthorizationRequestRepository;
import uz.pdp.appoauth2backend.security.oauth2.OAuth2AuthenticationFailureHandler;
import uz.pdp.appoauth2backend.security.oauth2.OAuth2AuthenticationSuccessHandler;
import uz.pdp.appoauth2backend.service.CustomOAuth2UserService;
import uz.pdp.appoauth2backend.utils.AppConstants;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        jsr250Enabled = true,
        securedEnabled = true)
@RequiredArgsConstructor
public class SecurityConfig {

    private final HttpCookieOAuth2AuthorizationRequestRepository httpCookieOAuth2AuthorizationRequestRepository;
    private final CustomOAuth2UserService customOAuth2UserService;

    private final OAuth2AuthenticationSuccessHandler auth2AuthenticationSuccessHandler;
    private final OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
                .cors()
                .and()
                .csrf()
                .disable()
                .authorizeHttpRequests(f -> {
                            try {
                                f
                                        .requestMatchers(AppConstants.OPEN_PAGES)
                                        .permitAll()
                                        .requestMatchers("/*")
                                        .permitAll()
                                        .requestMatchers(AppConstants.BASE_PATH + "/**")
                                        .authenticated()
                                        .and()
                                        .oauth2Login()
                                        .authorizationEndpoint()
                                        .baseUri("/oauth2/authorize")
                                        .authorizationRequestRepository(httpCookieOAuth2AuthorizationRequestRepository)
                                        .and()
                                        .redirectionEndpoint()
                                        .baseUri("/oauth2/callback/*")
                                        .and()
                                        .userInfoEndpoint()
                                        .userService(customOAuth2UserService)
                                        .and()
                                        .successHandler(auth2AuthenticationSuccessHandler)
                                        .failureHandler(oAuth2AuthenticationFailureHandler);
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }


                )
                .build();

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }


}
