package uz.pdp.appoauth2backend.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import uz.pdp.appoauth2backend.payload.ApiResponse;
import uz.pdp.appoauth2backend.payload.SignInDTO;
import uz.pdp.appoauth2backend.payload.TokenDTO;
import uz.pdp.appoauth2backend.service.AuthService;

import java.util.Date;

@Component
public class JWTProvider {
    @Value("${app.jwt.token.access.key}")
    private String ACCESS_TOKEN_KEY;

    @Value("${app.jwt.token.access.expiration}")
    private Long ACCESS_TOKEN_EXPIRATION;

    @Value("${app.jwt.token.refresh.key}")
    private String REFRESH_TOKEN_KEY;

    @Value("${app.jwt.token.refresh.expiration}")
    private Long REFRESH_TOKEN_EXPIRATION;

    public String generateAccessToken(UserPrincipal userPrincipal) {


        Date accessExpirationDate = new Date(System.currentTimeMillis()
                + ACCESS_TOKEN_EXPIRATION);

        return Jwts
                .builder()
                .signWith(SignatureAlgorithm.HS256, ACCESS_TOKEN_KEY)
                .setSubject(userPrincipal.getId().toString())
                .setIssuedAt(new Date())
                .setExpiration(accessExpirationDate)
                .compact();

    }

    public String generateRefreshToken(UserPrincipal userPrincipal) {
        Date refreshExpirationDate = new Date(System.currentTimeMillis()
                + REFRESH_TOKEN_EXPIRATION);

        return Jwts
                .builder()
                .signWith(SignatureAlgorithm.HS256, REFRESH_TOKEN_KEY)
                .setSubject(userPrincipal.getId().toString())
                .setIssuedAt(new Date())
                .setExpiration(refreshExpirationDate)
                .compact();

    }
}