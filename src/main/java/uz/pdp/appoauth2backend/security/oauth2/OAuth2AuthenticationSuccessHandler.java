package uz.pdp.appoauth2backend.security.oauth2;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;
import uz.pdp.appoauth2backend.repository.UserRepository;
import uz.pdp.appoauth2backend.security.JWTProvider;
import uz.pdp.appoauth2backend.security.UserPrincipal;
import uz.pdp.appoauth2backend.utils.AppConstants;
import uz.pdp.appoauth2backend.utils.CookieUtils;

import java.io.IOException;
import java.util.Optional;

import static uz.pdp.appoauth2backend.security.oauth2.HttpCookieOAuth2AuthorizationRequestRepository.REDIRECT_URI_SIGN_PARAM_COOKIE_NAME;


@Component
@RequiredArgsConstructor
public class OAuth2AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private final JWTProvider jwtProvider;
    private final UserRepository userRepository;
    private final HttpCookieOAuth2AuthorizationRequestRepository httpCookieOAuth2AuthorizationRequestRepository;


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException {

        String targetUrl = determineTargetUrl(request, response, authentication);

        if (response.isCommitted()) {
            logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }

        clearAuthenticationAttributes(request, response);
        getRedirectStrategy()
                .sendRedirect(request, response, targetUrl);
    }

    protected String determineTargetUrl(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) {
        Optional<String> optionalSignRedirectUri = CookieUtils
                .getCookie(request, REDIRECT_URI_SIGN_PARAM_COOKIE_NAME)
                .map(Cookie::getValue);


        String targetUrl = null;
        String token = null;
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        if (optionalSignRedirectUri.isPresent())
            targetUrl = optionalSignRedirectUri.orElse(getDefaultTargetUrl());


        String accessToken;
        String refreshToken;
        accessToken = jwtProvider.generateAccessToken(userPrincipal);
        refreshToken = jwtProvider.generateRefreshToken(userPrincipal);
        assert targetUrl != null;

        return UriComponentsBuilder.fromUriString(targetUrl)
                .queryParam(AppConstants.ACCESS_TOKEN, AppConstants.AUTH_TYPE_BEARER + accessToken)
                .queryParam(AppConstants.REFRESH_TOKEN, refreshToken)
                .build().toUriString();
    }

    protected void clearAuthenticationAttributes(HttpServletRequest request, HttpServletResponse response) {
        super.clearAuthenticationAttributes(request);
        httpCookieOAuth2AuthorizationRequestRepository.removeAuthorizationRequestCookies(request, response);
    }
}